﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_GestionTareas.Models
{
    public class GestionContext : DbContext
    { 

    public GestionContext()
    {
    }
    public GestionContext(DbContextOptions<GestionContext> options)
      : base(options)
    {
    }

    public static string GetConnectionString()
    {
        return Startup.Gestionconn;
    }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        if (!optionsBuilder.IsConfigured)
        {
            var con = GetConnectionString();
            optionsBuilder.UseSqlServer(con);
        }
    }

    #region DataSet 


    public virtual DbSet<SP_ConsultarTareas_Result> Consultar_Tareas { get; set; }

    #endregion

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
    }
}
}
