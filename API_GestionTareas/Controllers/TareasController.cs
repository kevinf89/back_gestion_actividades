﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using API_GestionTareas.Models;
using API_GestionTareas.Models.Otros;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace API_GestionTareas.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class TareasController : ControllerBase
    {
        /// <summary>
        /// Get a list with all the user tasks
        /// </summary>
        /// <param name="IdUsuario">user id</param>
        /// <returns>A tasks list</returns>
        [Route("~/api/ConsTareasEmpleado")]
        [HttpGet]
        public RespuestaGestion ConsTareasEmpleado(int IdUsuario)
        {
            RespuestaGestion Respuesta = new RespuestaGestion();
            var context = new GestionContext();
            List<SP_ConsultarTareas_Result> Tareas;
            try
            {
                object[] parameters = new[] { IdUsuario.ToString() };
                Tareas = context.Consultar_Tareas.FromSql("[dbo].[SP_ConsultarTareas] @p0", parameters: parameters).ToList();

                if (Tareas.Count > 0)
                {
                    Respuesta.Valido = true;
                    Respuesta.Tareas = Tareas;
                }
                else {
                    Respuesta.Valido = true;
                    Respuesta.Descripcion = "Este Empleado no tiene tareas";
                }
            }
            catch (Exception ex) {

                Respuesta.Valido = false;
                Respuesta.Error = ex.Message;
            }
            return Respuesta;
        }
    }
}